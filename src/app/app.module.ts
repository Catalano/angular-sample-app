import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SampleProducts } from './containers/products/products.component';
import { SampleProduct } from './components/product/product.component';
import { DetailsComponent } from './containers/details/details.component';
import { ProductDialogFormComponent } from './components/product-dialog-form/product-dialog-form.component';

import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatButtonModule, MatCardModule, MatDialogModule, MatTooltipModule, MatProgressBarModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    AppComponent,
    SampleProducts,
    SampleProduct,
    DetailsComponent,
    ProductDialogFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatTooltipModule,
    MatProgressBarModule,
    MatFormFieldModule,
    MatInputModule
  ],
  entryComponents: [
    ProductDialogFormComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
