import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SampleProducts } from './containers/products/products.component';
import { DetailsComponent } from './containers/details/details.component';


const routes: Routes = [
  { path: '', component: SampleProducts },
  { path: 'product/:id', component: DetailsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
