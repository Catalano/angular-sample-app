import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http"
import { Observable } from 'rxjs';
import { delay } from "rxjs/operators";


@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  readonly apiUrl = 'http://localhost:3000/products';
  
  constructor(private http: HttpClient){}
  
  getProducts() : Observable<IProduct[]> {
    return this.http.get<IProduct[]>(this.apiUrl);
  }

  getDetails(id: number) : Observable<IProduct> {
    return this.http.get<IProduct>(`${this.apiUrl}/${id}`).pipe(delay(1000)); // delay to see the loading screen
  }

  addProduct(product: IProduct) : Observable<any> {
    return this.http.post(this.apiUrl, product);
  }
}