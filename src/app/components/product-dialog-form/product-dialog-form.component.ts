import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'product-dialog-form',
  templateUrl: './product-dialog-form.component.html',
  styleUrls: ['./product-dialog-form.component.scss']
})
export class ProductDialogFormComponent {
  newProduct: IProduct = {
    id: null,
    name: '',
    details: ''
  }

  constructor(
    public dialogRef: MatDialogRef<ProductDialogFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
    ) {}

    onNoClick() : void {
      this.dialogRef.close();
    }

    saveProduct() {
      this.dialogRef.close(this.newProduct);
    }
}