import { Component, OnInit, Input } from "@angular/core";

@Component({
    selector: 'sample-product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.scss']
})
export class SampleProduct implements OnInit {
    @Input() product: IProduct;

    ngOnInit(){}
}