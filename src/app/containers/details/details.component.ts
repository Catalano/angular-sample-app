import { Component, OnInit, Input } from "@angular/core";
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ProductsService } from 'src/app/services/products.service';

@Component({
    templateUrl: 'details.component.html',
    styleUrls: ['details.component.scss']
})
export class DetailsComponent implements OnInit {
    @Input() id: number;
    _product$: Observable<IProduct>;

    constructor(
        private productService: ProductsService,
        private route: ActivatedRoute,
        private location: Location // TODO: goback button
    ) {}
    
    ngOnInit() {
        this.getProduct();
    }

    getProduct() {
        const id = +this.route.snapshot.paramMap.get('id');
        this._product$ = this.productService.getDetails(id);
    }
}