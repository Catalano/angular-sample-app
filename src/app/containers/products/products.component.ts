import { Component, OnInit } from "@angular/core";
import { Observable } from 'rxjs';
import { ProductsService } from '../../services/products.service';
import { MatDialog } from '@angular/material';
import { ProductDialogFormComponent } from 'src/app/components/product-dialog-form/product-dialog-form.component';
import { element } from 'protractor';

@Component({
  templateUrl: 'products.component.html',
  styleUrls: ['products.component.scss']
})
export class SampleProducts implements OnInit {
  _products$: Observable<IProduct[]>;
  
  constructor(
    public dialog: MatDialog,
    private productsService: ProductsService
  ) {}
  
  ngOnInit() {
    this._products$ = this.productsService.getProducts();
  }
  
  openDialog(): void {
    const dialogRef = this.dialog.open(ProductDialogFormComponent, {
      width: '450px'
    });
    dialogRef.afterClosed().subscribe((result: IProduct) => {
      console.log('The dialog was closed');
      if (!!result) {
        console.log('saving product: ', result);
        this.productsService.addProduct(result)
          .subscribe(_ => {
            console.log('saved!');
            this._products$ = this.productsService.getProducts();
          });
      }
    });
  }
}